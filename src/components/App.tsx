import { Provider } from 'react-redux';
import { store } from '../context';
import ReposList from './ReposList';

const App = () => {
  return (
    <Provider store={store}>
      <div>
        <h1>search for package</h1>
        <ReposList />
      </div>
    </Provider>
  );
};

export default App;
