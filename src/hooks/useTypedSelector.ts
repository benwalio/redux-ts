import { useSelector, TypedUseSelectorHook } from 'react-redux';
import { RootState } from '../context';

export const useTypedSelector: TypedUseSelectorHook<RootState> = useSelector;
